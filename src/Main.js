import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import AddTodo from './containers/AddTodo';
import VisibleTodos from './containers/VisibleTodos';
const Main = () => {
  state={
    todos:[],
    visibilityFilter:'SHOW_ALL_TODOS',
  }
  return (
    <>      
      <View style={styles.container}>
        <StatusBar barStyle = "light-content" 
        hidden = {false} 
        backgroundColor = "#43A047" 
        translucent = {true}/>
        <View style={styles.topBar}> 
          <Text style={styles.tittle}>
            To-Do List
          </Text>
        </View>
        <AddTodo/>
        <View>
          <VisibleTodos/>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  topBar:{
    padding:16,
    paddingTop:42,
    paddingBottom:8,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#2E7D32',

  },
  tittle:{
    color:'white',
    fontSize:26,    
  },
  
  
});

export default Main;

