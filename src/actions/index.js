let nextId=Math.floor((Math.random()*1000)+1);
export const addTodo =(text)=>({
    type:'ADD_TODO',
    id:nextId,
    text
})

export const toggleTodo =(id)=>({
    type:'TOOGLE_TODO',
    id
})
export const deleteTodo =(id)=>({
    type:'DELETE_TODO',
    id
})