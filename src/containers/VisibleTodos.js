import {connect} from 'react-redux';
import TodoList from '../components/TodoList';
import {toggleTodo} from '../actions';
//import {deleteTodo} from '../actions';

const mapStateProps=state=>({
    todos:state.todos
})

const mapDispatchToProps=dispatch=>({
    toggleTodo:id=>dispatch(
        toggleTodo(id)
    )
})


export default connect(mapStateProps,mapDispatchToProps)(TodoList)