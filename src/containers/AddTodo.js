import React,{Component, useReducer, useState}from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import {addTodo} from '../actions';

class AddTodo extends Component {
  //const [text,setText]=useState('');
  state={
    text:''
  }
  addTodo=(text)=>{
    
    //this.props.dispatch({type:'ADD_TODO',text})
    this.props.dispatch(addTodo(text))
    this.setState({text:''})
   
  }

  
  render(){
  return (
    <>      
      <SafeAreaView>
        {/* <ScrollView 
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          */}
          
          <View style={styles.inputContainer}>
            <TextInput placeholder={"Write something...."}
            onChangeText={(text)=>this.setState({text})}
            style={styles.input}
            value={this.state.text}/>
            <TouchableOpacity onPress={()=>this.addTodo(this.state.text)}>
                <View style={styles.button}>
                    <Icon name='user-plus' size={30} style={styles.icon}/>
                </View>
            </TouchableOpacity>
          </View>
        {/* </ScrollView> */}
      </SafeAreaView>
    </>
  );
};
}

const styles = StyleSheet.create({
  button:{
    // height:50,
    // backgroundColor:'#eaeaea',
    // alignItems:'center',
    // justifyContent:'center',
      borderTopRightRadius:8,
      borderBottomRightRadius:8,
      backgroundColor:'white',
      height:36,
      flex:1,
  },
  icon:{
    color:'#1976D2',
    padding:0,
    marginRight:10,
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  bar:{
      flexDirection:'row',
      marginHorizontal:20,
  },
  // input:{
  //     borderWidth:1,
  //     borderColor:'red',
  //     backgroundColor:'#eaeaea',
  //     height:50,
  //     flex:1,
  //     padding:5,
  // },
  input:{      
      borderTopLeftRadius:8,
      borderBottomLeftRadius:8,
      backgroundColor:'white',
      height:36,
      flex:1,
      padding:5,
  },
  inputContainer:{
    padding:8,
    paddingTop:0,
    backgroundColor:'#2E7D32',
    flexDirection:'row',
  },
  
});

export default connect()(AddTodo);

