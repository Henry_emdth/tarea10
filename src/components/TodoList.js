import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


const TodoList = ({todos,toggleTodo}) => { 
   
  return (  
      <ScrollView>
        <View>
            {todos.map((todo,index)=>
                <TouchableOpacity key={index} 
                    onPress={()=> toggleTodo(todo.id)}
                    >
                    <View style={styles.list}>
                      <Text style={{padding:16,justifyContent:'space-between',
                                    alignItems:'center',
                        textDecorationLine: todo.completed ? 'line-through':'none'}         
                      }>
                          {todo.text}
                      </Text>
                      <Icon name='trash' size={30} style={styles.icon}/>
                    </View>
                </TouchableOpacity>
            )}        
        </View>
      </ScrollView>  
  );
};

const styles = StyleSheet.create({
  icon:{
    color:'red',
    padding:10,
    marginRight:10,
    width:50,
  },
  list:{
    flexDirection:'row',
    justifyContent: 'space-between',
    borderColor:'#ccc',
    borderTopWidth:1,
  },
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },  
  textContainer:{
      padding:16,
      //borderTopWidth:1,
      //borderBottomWidth:1,
      //marginBottom:-1,
      //borderColor:'#ccc',
     // flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  }  
});

export default TodoList;
