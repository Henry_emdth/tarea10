/**
 * @format
 */
import React,{Components} from 'react';
import {AppRegistry} from 'react-native';
import Main from './src/Main';
import store from './src/store';
import {Provider} from 'react-redux';
import {name as appName} from './app.json';

const reduxToDoList =()=>{
    return(
        <Provider store={store}>
            <Main/>

        </Provider>
    );
}

AppRegistry.registerComponent(appName, () => reduxToDoList);
